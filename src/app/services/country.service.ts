import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from '../../environments/environment';
import { map, catchError } from 'rxjs/operators';
import { AppError } from '../Common/app-error';
import { NotFoundError } from '../Common/not-found-error';
import { BadRequestError } from '../Common/bad-request-error';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CountryService {
  private url = environment.geoGuardProAPI; // 'http://localhost:50261/api/Global/Countries/';

  /* API ACTIONS */
  private GETALL = "GetAll";
  private ADDITEM = "AddItem";
  private EDITITEM = "UpdItem";
  private DELITEM = "DelById";

  constructor(private http: Http) {

  }

  getCountries() {
    return this.http.get(this.url + this.GETALL)
      .pipe(
        map(response => response.json()),
        catchError(this.handlerError));
  }

  createCountry(country) {
    return this.http.post(this.url + this.ADDITEM, country)
      .pipe(
        map(response => response.json()),
        catchError(this.handlerError)
      );
  }

  updateCountry(country) {
    return this.http.put(this.url + this.EDITITEM + "/" + country.id, country);
  }

  deleteCountry(id) {
    return this.http.delete(this.url + this.DELITEM + "/" + id);
  }

  private handlerError(error: Response) {
    console.log('personal error', error);
    if (error.status === 404) {
      return throwError(new NotFoundError());
    }

    if (error.status === 400) {
      return throwError(new BadRequestError(error.toString()));
    }

    return throwError(new AppError(error));
  }
}
