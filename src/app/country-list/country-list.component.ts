import { Component, OnInit } from '@angular/core';
import { CountryService } from '../services/country.service';
import { AppError } from '../Common/app-error';
import { BadRequestError } from '../Common/bad-request-error';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { CountryFormComponent } from '../country-form/country-form.component';

@Component({
  selector: 'app-country-list',
  templateUrl: './country-list.component.html',
  styleUrls: ['./country-list.component.css']
})
export class CountryListComponent implements OnInit {
  countries: any[];
  ErrorLabel: string;
  SuccessLabel: string;
  closeResult: string;

  constructor(private service: CountryService, private modalService: NgbModal) {

  }
  updateCountry(country) {
    country.name += " Updated";
    this.service.updateCountry(country)
      .subscribe(response => {
        console.log(response);
      });
  }
  deleteCountry(country) {
    this.service.deleteCountry(country.id)
      .subscribe(response => {
        const index = this.countries.indexOf(country);
        this.countries.splice(index, 1);
      });
  }

  createCountry(input: HTMLInputElement) {
    const country = {
      name: input.value,
      common_name: input.value,
      status: 'Active',
      entityType: 'Country',
      alpha_2_code: 'AB',
      alpha_3_code: 'ABC',
      numeric_code: '123',
      _rid: "",
      _self: ""
    };
    input.value = "";

    this.service.createCountry(country)
      .subscribe(response => {
        this.countries.push(response.json());
        console.log("country", response.json());
      },
      (error: AppError) => {
        this.countries.pop();

        if ( error instanceof BadRequestError ) {
          // alert('Bad request from server' + error.originalError);
          this.ErrorLabel = 'Bad request from server' + error.originalError;
          setTimeout(() => {
            this.ErrorLabel = '';
          }, 4000);
        } else throw error;

      } );
  }
  ngOnInit() {
      this.ErrorLabel = '';
      this.service.getCountries()
        .subscribe(response => {
          this.countries = response;
        }
      );
  }

  open() {
    const modalRef = this.modalService.open(CountryFormComponent);
    modalRef.componentInstance.name = 'world';
    modalRef.result.then(
      result => {
        this.countries.push(result);

        this.SuccessLabel = 'Country inserted in DB';
          setTimeout(() => {
            this.SuccessLabel = '';
          }, 4000);
      }
    );
    /*this.modalService.open(CountryFormComponent, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });*/
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
}
