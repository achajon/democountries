import { Component, OnInit, Input } from '@angular/core';
import { CountryService } from '../services/country.service';
import { NgForm } from '../../../node_modules/@angular/forms';
import { NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-country-form',
  templateUrl: './country-form.component.html',
  styleUrls: ['./country-form.component.css']
})
export class CountryFormComponent implements OnInit {
  @Input() name;
  constructor(private service: CountryService, public activeModal: NgbActiveModal) {

  }

  onSubmit(f: NgForm) {
    if (f.valid) {
      const country = {
        name: f.value.name,
        common_name: f.value.common_name,
        status: 'Active',
        entityType: 'Country',
        alpha_2_code: f.value.alpha_2_code,
        alpha_3_code: f.value.alpha_3_code,
        numeric_code: f.value.numeric_code,
        _rid: "",
        _self: ""
      };

      this.service.createCountry(country)
        .subscribe(response => {
          this.activeModal.close(response);
          console.log("CreateCountry", response);
        } );
    }
  }

  ngOnInit() {
  }
}
