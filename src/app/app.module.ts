import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { CountryListComponent } from './country-list/country-list.component';
import { CountryService } from './services/country.service';
import {NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CountryFormComponent } from './country-form/country-form.component';

@NgModule({
  declarations: [
    AppComponent,
    CountryListComponent,
    CountryFormComponent
  ],
  entryComponents: [ CountryFormComponent],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    NgbModule.forRoot()
  ],
  providers: [ CountryService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
